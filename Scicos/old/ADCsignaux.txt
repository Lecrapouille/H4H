ADC pins / Mot port serie (dsPIC --> PC):
==========================================
----------------------------
Mot 1 | AN0 | pin 2 | CH0 |
----------------------------
Mot 2 | AN3 | pin 5 | CH1 |
----------------------------
Acc X | AN5 | pin 7 | CH3 |
----------------------------
Gyr X | AN4 | pin 6 | CH2 |
----------------------------

Ordre conversions: M1 M2 Gx Ax


// OLD:
RB0 --> 1: Fenetre 1 Noir: Crt Moteur 1
RB1 --> X (Esclave: gyro Z / Maitre: Altimetre )
RB2 --> /SS
RB3 --> 2: Fenetre 1 Vert: Crt Moteur 2
RB4 --> 3: Fenetre 2 Noir: Accelero X
RB5 --> 4: Fenetre 2 Vert: Gyro X