BootloaderMessages.txt helper:
==============================

!!!! ORDRE DES MESSAGES A RESPECTER !!!!!
Message 1: PID asservissement courant du moteur Avant + coef de la fonction i2PWM
Message 2: PID asservissement courant du moteur Arriere + coef de la fonction i2PWM
Message 3: PID asservissement attitude : angle du tangage
Message 4: PID asservissement courant du moteur Droit + coef de la fonction i2PWM
Message 5: PID asservissement courant du moteur Gauche + coef de la fonction i2PWM
Message 6: PID asservissement attitude : angle du roulis
Message 7: PID asservissement attitude : angle du lacet et de l'attitude

Note 1: Tous ces parametres sont precedes du nombre 1 ou 0 indiquant
que le regulateur en question doit etre actif (1) ou non (0).

Note 2: Le bootloader va d'abord envoyer le message 0 indiquant le
nombre de messages qui va etre envoyes. Il ajoute aussi le numero de
la ligne dans le message. Apres avoir les dits messages, il en envoie
un dernier pour indiquer l'arret du bootloader.

Note 3: La taille des messages doit etre de 9. Si le message a envoyer
est inferieur a ce nombre il faut ajouter autant de 0 que necessaire
jusqu'a atteindre la longueur d'un message.

Note 4: Pour l'instant l'ordre des messages est important : ne pas
melanger les messages.
