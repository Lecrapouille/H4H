;
; h4h.asm for H4H in /Users/crapouille
;
; Made by Quentin Quadrat
; Mail   <quentin.quadrat@free.fr>
;
; Started on  Thu Jun 29 17:35:11 2006 Quentin Quadrat
; Last update Wed Sep 13 14:23:51 2006 Quentin Quadrat
;



	LIST      p=16F876A            ; D�finition de processeur
	#include <p16F876A.inc>        ; fichier include

	__CONFIG   _CP_OFF & _DEBUG_OFF & _CPD_OFF & _LVP_OFF & _BODEN_OFF & _PWRTE_ON & _WDT_ON & _HS_OSC

	;_CP_OFF	 Pas de protection
	;_DEBUG_OFF	 RB6 et RB7 en utilisation normale
	;_WRT_ENABLE_OFF Le programme ne peut pas ecrire dans la flash
	;_CPD_OFF        Memoire EEprom deprotegee
	;_LVP_OFF	 RB3 en utilisation normale
	;_BODEN_ON	 Reset tension en service
	;_PWRTE_ON	 Demarrage temporise
	;_WDT_ON	 Watchdog en service
	;_HS_OSC	 Oscillateur haute vitesse (4Mhz<F<20Mhz)

	;
; macros.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:03:51 2006 Quentin Quadrat
; Last update Wed Sep 13 15:52:51 2006 Quentin Quadrat
;

;*****************************************************************************
;                               ASSIGNATIONS SYSTEME                         *
;*****************************************************************************

		; REGISTRE OPTION_REG (configuration)
		; -----------------------------------
OPTIONVAL	EQU	B'10001111'
			; RBPU     b7 : 1= R�sistance rappel +5V hors service
			; PSA      b3 : 1= Assignation pr�diviseur sur Watchdog
			; PS2/PS0  b2/b0 valeur du pr�diviseur = 256

		; REGISTRE INTCON (contr�le interruptions standard)
		; -------------------------------------------------
INTCONVAL	EQU	B'01100000'
			; GIE	b7 : activation generale interruptions
			; PEIE	b6 : masque autorisation g�n�rale p�riph�riques
			; T0IE	b5 : Timer 0
			; RBIE	b3 : Int chgt de niv RB4 --> RB7

		; REGISTRE PIE1 (contr�le interruptions p�riph�riques)
		; ----------------------------------------------------
PIE1VAL		EQU	B'01000010'
			; ADIE      b6 : masque interrupt convertisseur A/D
			; TMR2IE    b1 : masque interrupt TMR2 = PR2

		; REGISTRE PIE2 (contr�le interruptions p�riph�riques)
		; ----------------------------------------------------
PIE2VAL		EQU	B'00001000'
			; BCLIE    b3 : masque interrupt SSP mode I2C

		; REGISTRE ADCON1 (ANALOGIQUE/DIGITAL)
		; ------------------------------------
ADCON1VAL	EQU	B'10000000'
			; RA0/AN0 --> RA5/AN4 (sauf RA4) en analogique
		    	; justification � droite

;*****************************************************************************
;                           ASSIGNATIONS PROGRAMME                           *
;*****************************************************************************
OFFSETL	 EQU 	0x00		; valeur mini de CCPRx en quarts de valeurs
OFFSETH	 EQU 	0x00		; valeur mini de CCPRx en quarts de valeurs
				; OFFSET = OFFSETH::OFFSETL = 1023

	;; TIMER 2 pour generation signal PWM a 5kHz
	;; -----------------------------------------
PR2VAL	 EQU	0xFF	; valeur d'initialisation de PR2 (a muliplier par prediciseur)
			; Nous, dans ce projet on veut que le timer deborde a la valeur
			; de 1023 (0xFF * prediv 4).

SLAVE_ADDRESS	EQU	0x30 	; adresse i2c esclave

;*****************************************************************************
;                             MACRO                                          *
;*****************************************************************************

			; Changement de banques
			; ----------------------

BANK0	macro				; passer en banque0
		bcf	STATUS,RP0
		bcf	STATUS,RP1
	endm

BANK1	macro				; passer en banque1
		bsf	STATUS,RP0
		bcf	STATUS,RP1
	endm

BANK2	macro				; passer en banque2
		bcf	STATUS,RP0
		bsf	STATUS,RP1
	endm

BANK3	macro				; passer en banque3
		bsf	STATUS,RP0
		bsf	STATUS,RP1
	endm

	;
; variables.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:10:42 2006 Quentin Quadrat
; Last update Wed Sep 13 11:45:40 2006 Quentin Quadrat
;

;*****************************************************************************
;                        VARIABLES BANQUE 0                                  *
;*****************************************************************************

; Zone de 80 bytes
; ----------------

	CBLOCK	0x20		; D�but de la zone (0x20 � 0x6F)
	octemp : 1		; sauvegarde temporaire
	octemp1 : 1		; sauvegarde temporaire
 	zonevalH : 1		; copie temp valeur SCICOSPWMxH (poids fort)
 	zonevalL : 1		; copie temp valeur SCICOSPWMxL (poids faible)
 	ccpcon:	1		; copie temp de CCP1CON ou CCP2CON
        ENDC			; Fin de la zone

;*****************************************************************************
;                      VARIABLES ZONE COMMUNE                                *
;*****************************************************************************

; Zone de 16 bytes
; ----------------

	CBLOCK 0x70		; D�but de la zone (0x70 � 0x7F)
	w_temp : 1		; Sauvegarde registre W
	status_temp : 1		; sauvegarde registre STATUS
	FSR_temp : 1		; sauvegarde FSR (si indirect en interrupt)
	PCLATH_temp : 1		; sauvegarde PCLATH (si prog>2K)
	bufinptr : 1		; pointeur sur caract�re courant buffer entr�e
	bufoutptr : 1		; pointeur sur caract�re courant buffer sortie
	ENDC

;*****************************************************************************
;                        VARIABLES BANQUE 2                                  *
;*****************************************************************************

; Zone de 96 bytes
; ----------------

	CBLOCK	0x110		; D�but de la zone (0x110 � 0x16F)
	bufin : D'96'		; zone de stockage des donn�es entr�es (I2C)

	ENDC			; Fin de la zone

;*****************************************************************************
;                        VARIABLES BANQUE 3                                  *
;*****************************************************************************

; Zone de 96 bytes
; ----------------

	CBLOCK	0x190		; D�but de la zone (0x190 � 0x1EF)
	bufout : D'96'		; message � envoyer (I2C)
	ENDC			; Fin de la zone


	org	0x000 		; Adresse de d�part apr�s reset
 	goto    init		; Initialiser

	;
; interruption.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:21:45 2006 Quentin Quadrat
; Last update Wed Sep 13 15:08:23 2006 Quentin Quadrat
;

;*****************************************************************************
;                     ROUTINE INTERRUPTION                                   *
;*****************************************************************************
;-----------------------------------------------------------------------------
; La suppression des lignes "goto restorereg" permet dans ce cas de traiter
; l'interruption sur r�ception et sur �mission en une seule fois
;-----------------------------------------------------------------------------

	;sauvegarder registres
	;---------------------
	org 0x004		; adresse d'interruption
	movwf   w_temp  	; sauver registre W
	swapf	STATUS,w	; swap status avec r�sultat dans w
	movwf	status_temp	; sauver status swapp�
	movf	FSR , w		; charger FSR
	movwf	FSR_temp	; sauvegarder FSR

	; switch vers diff�rentes interrupts
	;-----------------------------------

			; Interruption I2C
			; ----------------
intsw0
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE2,BCLIE	; tester si interrupt autoris�e
	goto 	intsw1		; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR2,BCLIF	; oui, tester si interrupt en cours
	goto 	intsw1		; non sauter
	call	intssp		; oui, traiter interrupt
	bcf	PIR2,BCLIF	; Effacer flag interruption

			; Interruption convertisseur A/D
			; -------------------------------
intsw1
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE1,ADIE	; tester si interrupt autoris�e
	goto 	intsw2		; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR1,ADIF	; oui, tester si interrupt en cours
	goto 	intsw2		; non sauter
	call	intad		; oui, traiter interrupt
	bcf	PIR1,ADIF 	; effacer flag interupt

			; Interruption Interruption TMR0
			; ------------------------------
intsw2
	btfss	INTCON,T0IE	; tester si interrupt autoris�e
	goto 	intsw3		; non sauter
	btfss	INTCON,T0IF	; oui, tester si interrupt en cours
	goto 	intsw3		; non sauter
	call	inttmr0		; oui, traiter interrupt
	bcf	INTCON,T0IF	; Effacer flag interruption

			; Interruption Interruption TMR2
			; ------------------------------
intsw3
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE1,TMR2IE	; tester si interrupt autoris�e
	goto 	restorereg	; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR1,TMR2IF	; oui, tester si interrupt en cours
	goto 	restorereg	; non sauter
	call	inttmr2		; oui, traiter interrupt
	bcf	PIR1,TMR2IF	; Effacer flag interruption


			; Restaurer registres
			;-------------------
restorereg
	movf	FSR_temp , w	; charger FSR sauv�
	movwf	FSR		; restaurer FSR
	swapf	status_temp,w	; swap ancien status, r�sultat dans w
	movwf   STATUS		; restaurer status
	swapf   w_temp,f	; Inversion L et H de l'ancien W
                       		; sans modifier Z
	swapf   w_temp,w  	; R�inversion de L et H dans W
				; W restaur� sans modifier status
	retfie  		; return from interrupt

	;
; anc.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Sat Jul 15 17:54:01 2006 Quentin Quadrat
; Last update Tue Sep 12 19:08:53 2006 Quentin Quadrat
;

;*****************************************************************************
;                     INTERRUPTION CONVERTISSEUR A/D                         *
;*****************************************************************************
;-----------------------------------------------------------------------------
; Copier le resultat de la conversion analogique dans le buffer de sortie du
; port serie et change de patte analogique. Il faut attendre que les condensateurs
; se chargent avant de lancer une nouvelle conversion (utilisation Timer 0).
;-----------------------------------------------------------------------------
intad
	clrwdt	; effacer watch dog

			; Lire dans ADCON0, le numero de la derniere
			; patte analogique utilisee et multiplier par
			; 2 le resultat. Resultat final est dans octemp.
			; ----------------------------------------------
	clrf	octemp
	btfsc	ADCON0,3
	bsf	octemp,1 ; decalage de 1 sur la gauche <==> multiplication par 2
	btfsc	ADCON0,4
	bsf	octemp,2
	btfsc	ADCON0,5
	bsf	octemp,3

			; Changer de pattes analogique (ADCON0)
			; -------------------------------------
	btfss	ADCON0,5	; Tester si patte == RA5/AN4
	goto	intad00
	goto	intad01
intad00
	movlw	B'00001000'	; mask permettant passer a la patte analog suivante
	addwf	ADCON0,f	; ADCON0 := ADCON0 + B'00001000' (le b0 est deja a 1)
	goto	intadend
intad01
	movlw	B'10000001'	; mask permettant de cycler sur patte RA0/AN0
	movwf	ADCON0		; ADCON0 := B'10000001'
intadend

			; Remplissage buffer correspond a la patte precedente
			; ---------------------------------------------------
	bsf	STATUS,IRP	; pointer banques 2 et 3 en indirect
	movlw	LOW bufout
	addwf	octemp,w	; charger pointeur bufout + analog Channel Select
	movwf	FSR		; dans pointeur d'adresse


			; Sauvegarde des bits de poids forts dans buffout
			; ------------------------------------------------
	movf	ADRESH,w	; copier result analog dans W (poids fort)
	movwf	INDF		; sauver dans buffer
	incf	FSR,f		; pointeur boufout += 1

			; Sauvegarde des bits de poids faibles dans buffout
			; Attention la valeur 0xFF est reservee pour EOL.
			; ------------------------------------------------
	movlw	EOL		; Copier EOL dans reg temp pour
	movwf	octemp1		; gestion du conflit ADRESL == EOL
	BANK1
	movf	ADRESL,w	; copier result analog dans W (poids faible)
	BANK0
	xorwf	octemp1,f	; Tester si ADRESL == EOL. Resultat est dans Z
	btfsc	STATUS,Z	; tester si w == EOL (valeur reservee).
	movlw	EOL-0x01	; Si ADRESL == EOL alors w := ADRESL - 1
	movwf	INDF		; Pas de probleme ADRESL != EOL donc pas de conflit

	return

	;
; timer.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Sat Jul 15 17:58:04 2006 Quentin Quadrat
; Last update Tue Sep 12 18:31:05 2006 Quentin Quadrat
;

;*****************************************************************************
;                     INTERRUPTION TIMER 0                                   *
;*****************************************************************************
; La conversion A/D est relancee a chaque interruption Timer 0
inttmr0
	bsf	ADCON0,GO
	return

;*****************************************************************************
;                     INTERRUPTION TIMER 2                                   *
;*****************************************************************************
; Fin des cycles PWM generes par les pattes CCP1 et CCP2.
; Changement du rapport cyclique du prochain signal PWM (rapport entre le temps
; de l'etat haut et le temps de l'etat bas).
;
inttmr2
	clrwdt	; effacer watch dog

	; Calcul PWM du CCP1
	; ==================
			; Copier SCICOSPWM1H dans zonevalH
			; --------------------------------
	BANKSEL bufin
	movf	SCICOSPWM1H,w
	BANK0
	movwf	zonevalH

			; Copier SCICOSPWM1L dans zonevalL
			; --------------------------------
	BANKSEL bufin
	movf	SCICOSPWM1L,w
	BANK0
	movwf	zonevalL

			; initialiser bits fractionnaires
			; -------------------------------
	bcf	CCP1CON,CCP1Y	; effacer bit 0
	bcf	CCP1CON,CCP1X	; effacer bit 1

			; Copier CCP1CON dans ccpcon
			; --------------------------
	movf	CCP1CON,w
	movwf	ccpcon
	call	calculpulse	; Calculer la nouvelle pulsation PWM

			; placer nouvelle valeur de consigne
			; ----------------------------------
	movwf	CCPR1L		; resultat est dans W
				; copier resultat dans CCPR1L
	movf	ccpcon,w	; Restaurer CCP1CON
	movwf	CCP1CON		;


	; Calcul PWM du CCP2
	; ==================
			; Copier SCICOSPWM2H dans zonevalH
			; ----------------------------------
	BANKSEL bufin
	movf	SCICOSPWM2H,w
	BANK0
	movwf	zonevalH


			; Copier SCICOSPWM2H dans zonevalL
			; ----------------------------------
	BANKSEL bufin
	movf	SCICOSPWM2L,w
	BANK0
	movwf	zonevalL

			; initialiser bits fractionnaires
			; -------------------------------
	bcf	CCP2CON,CCP2Y	; effacer bit 0
	bcf	CCP2CON,CCP2X	; effacer bit 1

			; Copier CCP2CON dans ccpcon
			; --------------------------
	movf	CCP2CON,w
	movwf	ccpcon
	call	calculpulse	; Calculer la nouvelle pulsation PWM

			; placer nouvelle valeur de consigne
			; ----------------------------------
	movwf	CCPR2L
	movf	ccpcon,w
	movwf	CCP2CON

	return

calculpulse
		; OFFSET + zoneval : addition 10 bits
		; ===================================
			; Poids faible
			; ------------
	movlw	OFFSETL		; Mettre la val de l'Offset de poids faible
	addwf	zonevalL,f	; dans W et l'additionner a la valeur analogique
				; de poids faible

			; Poids fort
			; ----------
	btfsc	STATUS,C	; Additioner le Carry
	incf	zonevalH,f	; a la valeur analogique de poids fort

	movlw	OFFSETH		; Mettre la val de l'Offset de poids fort
	addwf	zonevalH,f	; dans W et l'additionner a la valeur analogique
				; de poids fort

	;; MAINTENANT AVEC OFFSET == 0x00 on a plus besoin de la /2
			; division sur 10 bits de 'zoneval' par 2
			; ---------------------------------------
	;; 	bcf 	STATUS,C	; Carry := 0
	;; 	rrf 	zonevalH,f  	; zonevalH := zonevalH / 2 + carry * 2^7
	;; 	rrf 	zonevalL,f	; zonevalL := zonevalL / 2 + carry * 2^7

	;; SI OFFSET == 0x03 0xFF alors decommenter le code /2

		; Placer valeur entiere sur 8 bits (div par 4)
		; ============================================

			; division par 2
			; --------------
	bcf 	STATUS,C	; Carry := 0
	rrf 	zonevalH,f  	; zonevalH := zonevalH / 2 + carry * 2^7
	rrf 	zonevalL,f	; zonevalL := zonevalL / 2 + carry * 2^7

			; sauver le premier bit fractionnaire
			; -----------------------------------
	btfsc	STATUS,C	; tester si futur bit "-1" = 1
	bsf	ccpcon,5	; oui, mettre bit 1 � 1

			; division par 2
			; --------------
	bcf 	STATUS,C	; Carry := 0
	rrf 	zonevalH,f  	; zonevalH := zonevalH / 2 + carry * 2^7
	rrf 	zonevalL,w	; w := zonevalL / 2 + carry * 2^7

			; sauver le premier bit fractionnaire
			; -----------------------------------
	btfsc	STATUS,C 	; tester si futur bit "-2" = 1
	bsf	ccpcon,4	; oui, mettre bit 0 � 1

	return

	;
; i2c.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Aug 24 01:04:46 2006 Quentin Quadrat
; Last update Wed Sep 13 11:43:22 2006 Quentin Quadrat
;

;*****************************************************************************
;*****************************************************************************
;                              ROUTINES I2C                                  *
;*****************************************************************************
;*****************************************************************************

;-----------------------------------------------------------------------------
; On attend que chaque commande soit termin�e avant de sortir de la
; sous-routine correspondante
;-----------------------------------------------------------------------------
IWAIT	macro	REGISTRE,BIT	; attendre effacement du bit du registre
	clrwdt			; effacer watchdog
	btfsc	REGISTRE,BIT	; bit effac�?
	goto	$-2		; non, attendre
	bcf	STATUS,RP0	; repasser en banque 0
	endm			; fin de macro

;*****************************************************************************
;                        ENVOYER LE START-CONDITION                          *
;*****************************************************************************
i2c_start
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,SEN	; lancer le start-condition
	IWAIT	SSPCON2,SEN	; attendre fin start-condition
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE REPEATED START-CONDITION                 *
;*****************************************************************************
i2c_rstart
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,RSEN	; lancer le repeated start-condition
	IWAIT	SSPCON2,RSEN	; attendre fin repeated start-condition
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE STOP-CONDITION                           *
;*****************************************************************************
i2c_stop
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,PEN	; lancer le stop-condition
	IWAIT	SSPCON2,PEN	; attendre fin stop-condition
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE ACK                                      *
;*****************************************************************************
i2c_ack
	bsf	STATUS,RP0	; passer en banque 1
	bcf	SSPCON2,ACKDT	; le bit qui sera envoy� vaudra " 0 "
	bsf	SSPCON2,ACKEN	; lancer l'acknowledge (= ACKDT = 0 = ACK)
	IWAIT	SSPCON2,ACKEN	; attendre fin ACK
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE NOACK                                    *
;*****************************************************************************
i2c_noack
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,ACKDT	; le bit qui sera envoy� vaudra " 1 "
	bsf	SSPCON2,ACKEN	; lancer l'acknowledge (= ACKDT = 1 = NOACK)
	IWAIT	SSPCON2,ACKEN	; attendre fin NOACK
	return			; et retour

;*****************************************************************************
;                          ENVOYER UN OCTET                                  *
;*****************************************************************************
;-----------------------------------------------------------------------------
; L'octet est pass� dans W
;-----------------------------------------------------------------------------
i2c_write
	movwf	SSPBUF		; lancer l'�mission de l'adresse en mode �criture
	bsf	STATUS,RP0	; passer en banque 1
	IWAIT	SSPSTAT,R_W	; attendre �mission termin�e
	return			; et retour

;*****************************************************************************
;                          LIRE UN OCTET                                     *
;*****************************************************************************
;-----------------------------------------------------------------------------
; L'octet est retourn� dans W
;-----------------------------------------------------------------------------
i2c_read
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,RCEN	; lancer la lecture
	IWAIT	SSPCON2,RCEN	; attendre r�ception termin�e
	movf	SSPBUF,w	; charger octet re�u
	return			; et retour
	;
; reponse.asm for hh in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Aug 24 00:54:41 2006 Quentin Quadrat
; Last update Wed Sep 13 15:20:10 2006 Quentin Quadrat
;

;*****************************************************************************
;                      PREPARER LA REPONSE                                   *
;*****************************************************************************

;*****************************************************************************
;			INTERRUPTION SSP en mode I2C
;*****************************************************************************
;-----------------------------------------------------------------------------
; Gestion de l'interruption en mode esclave.
; cas 1 : reception de l'adresse en mode ecriture
; cas 2 : reception d'une donnee
; cas 3 : reception de l'adresse en mode lecture
; cas 4 : envoi d'une donnee en mode lecture
; cas 5 : reception du NOACK de fin de lecture
; erreur : tout autre cas est incorrect, on provoque le reset du PIC par
; debordement du watchdog (a vous de traiter de facon appropriee)
;-----------------------------------------------------------------------------
intssp
			; conserver les bits utiles
			; --------------------------
	BANKSEL	SSPSTAT 	; passer en banque 1
	movf	SSPSTAT,w 	; charger valeur SSPSTAT
	andlw	B'00101101' 	; garder D_A, S, R_W, et BF
	bcf	STATUS,RP0	; repasser en banque 0
	movwf	ssptemp		; sauvegarder dans variable temporaire

		; cas 1 : reception adresse ecriture
		; ----------------------------------
	xorlw	B'00001001'	; buffer plein, contient une adresse
	btfss	STATUS,Z	; condition realisee ?
	goto	intssp2		; non, examiner cas 2
	movlw	LOW bufin	; charger adresse du buffer de reception
	movwf	ptrin		; le prochain octet sera le premier du buffer in
;; 	bcf	flaggen		; par defaut, pas une commande generale (broadcast)
	movf	SSPBUF,w	; efface BF, teste l'adresse
;; 	btfsc	STATUS,Z	; adresse generale recue ?
;; 	bsf	flaggen		; oui, positionner le flag (broadcast)
	return			; fin du traitement

		; cas 2 : reception d'une donnee
		; -------------------------------
intssp2
	movf	ssptemp,w	; charger bits utiles de SSPSTAT
	xorlw	B'00101001'	; buffer plein, contient une donnee
	btfss	STATUS,Z	; condition realisee ?
	goto	intssp3 	; non, examiner cas 3
	movf	ptrin,w 	; charger pointeur d'entree
	movwf	FSR 		; dans pointeur d'adresse
	movf	SSPBUF,w 	; charger donnee recue
	movwf	INDF 		; la placer dans le buffer d'entree
	incf	ptrin,f 	; incrementer le pointeur d'entree
	return 			; fin du traitement

		; cas 3 : emission de la premiere donnee
		; --------------------------------------
intssp3
	movf	ssptemp,w	; charger bits utiles de SSPSTAT
	xorlw	B'00001100'	; demande d'envoi, on vient de recevoir l'adresse
	btfss	STATUS,Z	; condition realisee ?
	goto	intssp4		; non, examiner cas 4
	movlw	LOW bufout+1 	; charger adresse du buffer d'emission + 1
	movwf	ptrout 		; le prochain octet sera le second du buffer out
	movf	bufout,w 	; charger le premier octet du buffer d'emission
	movwf	SSPBUF 		; dans le buffer I2C
	bsf	SSPCON,CKP 	; met fin a la pause, permet le transfert
	return 			; fin du traitement

		; cas 4 : emission d'une donnee quelconque
		; ----------------------------------------
intssp4
	movf	ssptemp,w 	; charger bits utiles de SSPSTAT
	xorlw	B'00101100' 	; demande d'envoi qui suit un autre envoi
	btfss	STATUS,Z 	; condition realisee ?
	goto	intssp5 	; non, examiner cas 5
	movf	ptrout,w 	; charger pointeur buffer de sortie
	movwf	FSR 		; dans pointeur d'adresse
	movf	INDF,w 		; charger octet a envoyer
	movwf	SSPBUF 		; le mettre dans le buffer de sortie
	bsf	SSPCON,CKP 	; libere l'horloge, permet le transfert
	incf	ptrout,f 	; incrementer pointeur de sortie
	return			; fin du traitement

		; cas 5 : reception du NOACK
		; ---------------------------
intssp5
	movf 	ssptemp,w	; charger bits utiles de SSPSTAT
	xorlw 	B'00101000'	; NOCAK recu
	btfss 	STATUS,Z	; condition realisee ?
	goto 	$ 		; non, reset PIC par debordement watchdog
	;; 	bsf 	flagout 	; signaler fin de lecture
	return 			; et retour

	;
; init.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Tue Sep 12 17:26:19 2006 Quentin Quadrat
; Last update Wed Sep 13 15:58:19 2006 Quentin Quadrat
;

;*****************************************************************************
;                          INITIALISATIONS                                   *
;*****************************************************************************

init
			; Registre d'options (banque 1)
			; -----------------------------
	BANK1			; s�lectionner banque 1
	movlw	OPTIONVAL	; charger masque
	movwf	OPTION_REG	; initialiser registre option

			; TRISx
			; -----
	bcf	TRISC,2		; CCP1/RC2 en sortie
	bcf	TRISC,1		; CCP2/RC1 en sortie

			; initialiser I�C (banque 1)
			; -------------------------
	;; FIXME: mettre address i�c maitre mettre trame serie

	movlw	SLAVE_ADDRESS	; valeur de l'adresse PIC esclave
	movwf	SSPADD		; dans registre d'adresse
	clrf	SSPSTAT		; slew rate control ON, compatible I�C
;;(facultatif)	movlw	B'10000000'	; appel broadcast
;; 	movlw	SSPCON2		;
	bcf	STATUS,RP0	; passer banque 0
	movlw	B'00110110'	; module MSSP en service, mode I�C esclave 7 bits, SCL ok
	movwf	SSPCON		; dans registre de contr�le
	BANK1

			; registres interruptions (banque 1)
			; ----------------------------------
	movlw	INTCONVAL	; charger valeur registre interruption
	movwf	INTCON		; initialiser interruptions
	movlw	PIE1VAL		; Initialiser registre
	movwf	PIE1		; interruptions p�riph�riques 1
	movlw	PIE2VAL		; Initialiser registre
	movwf	PIE2		; interruptions p�riph�riques 2

 			; Initialise les buffers
 			; ----------------------
	BANKSEL	bufin
 	movlw	0x00		; consignes PWM des 4 moteurs := 0
 	movwf	bufin+0x00	; moteur 1 (poids fort)
 	movwf	bufin+0x01	; moteur 1 (poids faible)
 	movwf	bufin+0x02	; moteur 2 (poids fort)
 	movwf	bufin+0x03	; moteur 2 (poids faible)
 	movwf	bufin+0x04	; moteur 3 (poids fort)
 	movwf	bufin+0x05	; moteur 3 (poids faible)
 	movwf	bufin+0x06	; moteur 4 (poids fort)
 	movwf	bufin+0x07	; moteur 4 (poids faible)

	BANKSEL	bufout
 	movlw	0x00		; 4 conversions analogiques (poids fort + faibles)
 	movwf	bufout+0x00	; dans bufout (==> buffer vide)
 	movwf	bufout+0x01	; dans bufout (==> buffer vide)
 	movwf	bufout+0x02	; dans bufout (==> buffer vide)
 	movwf	bufout+0x03	; dans bufout (==> buffer vide)
 	movwf	bufout+0x04	; dans bufout (==> buffer vide)
 	movwf	bufout+0x05	; dans bufout (==> buffer vide)
 	movwf	bufout+0x06	; dans bufout (==> buffer vide)
 	movwf	bufout+0x07	; dans bufout (==> buffer vide)
 	movwf	bufout+0x08	; dans bufout (==> buffer vide)
 	movwf	bufout+0x09	; dans bufout (==> buffer vide)
 	movwf	bufout+0x0A	; dans bufout (==> buffer vide)
	BANK0

			; initialiser variables
			; ---------------------
 	movlw	LOW bufin	; adresse du buffer de r�ception
 	movwf	bufinptr	; dans pointeur
 	movlw	LOW bufout	; adresse basse du buffer d'�mission
 	movwf	bufoutptr	; dans pointeur


			; configurer le module CCP1 et CCP2 et timer2
			; -------------------------------------------
	movlw	B'00001100'	; pour mode PWM
	movwf	CCP1CON		; dans registre de commande CCP1
	movwf	CCP2CON		; dans registre de commande CCP2
	movlw	PR2VAL		; valeur de d�bordement
	bsf	STATUS,RP0	; passer en banque 1
	movwf	PR2		; dans registre de comparaison Timer 2
	bcf	STATUS,RP0	; repasser en banque 0
	movlw	B'00000101'	; timer 2 on, pr�diviseur = 4
	movwf	T2CON		; dans registre de contr�le
				; LES SIGNAUX PWM SONT GENERES A 5kHz

			; configurer le convertisseur A/D
			; -------------------------------
	movlw	ADCON1VAL	; 1 entr�e analogique
	bsf	STATUS,RP0	; passer banque 1
	movwf	ADCON1		; �criture dans contr�le1 A/D
	bcf	STATUS,RP0	; repasser banque 0
	movlw	B'10000001'	; convertisseur ON, pr�diviseur 32
	movwf	ADCON0		; dans registre de contr�le 0

			; autoriser interruptions (banque 0)
			; ----------------------------------
	clrf	PIR1		; effacer flags 1 interruption
	clrf	PIR2		; effacer flags 2 interruption
	bsf	INTCON,GIE	; valider interruptions
	goto	start		; programme principal
	;
; mainloop.asm<2> for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Tue Jul 18 12:19:25 2006 Quentin Quadrat
; Last update Tue Sep 12 17:33:12 2006 Quentin Quadrat
;

;*****************************************************************************
;                      PROGRAMME PRINCIPAL                                   *
;*****************************************************************************

start
;;TRESQUE
;; 		; lancer l'�mission, message de bienvenue
;; 		; ---------------------------------------

;; 	bsf	STATUS,RP0	; passer en banque 1
;; 	bsf	TXSTA,TXEN	; �mission en service.
;; 	bsf	PIE1,TXIE	; envoyer message "pret"


;; 			; attendre fin de l'�mission
;; 			; --------------------------
;; 	clrwdt			; effacer watch dog
;; 	btfsc	PIE1,TXIE	; reste des caract�res � envoyer?
;; 	goto	$-2		; oui, attendre
;; 	btfss	TXSTA,TRMT	; buffer de sortie vide?
;; 	goto	$-4		; non, attendre

			; lancer la r�ception
			; --------------------
	bcf	STATUS,RP0	; passer banque 0
	bsf	RCSTA,CREN	; lancer la r�ception

			; revevoir le message
			; -------------------
	bsf	STATUS,RP0	; passer banque 1
loop
	bsf	PIE1,RCIE	; autoriser interruption r�ception
	clrwdt			; effacer watchdog
	btfsc	PIE1,RCIE	; tester si message complet re�u
	goto	$-2		; non, attendre


;;TRESQUE		; Recuperer les donnees du PIC esclave par SPI
;; 			; --------------------------------------------
;; 	;; 	call	datafromslave
;; 	BANK2
;; 	movf	bufin+0x04,w
;; 	BANK3
;; 	movwf	bufout+0x0A
;; 	BANK1

		; Attendre fin �mission pr�c�dente
		; --------------------------------
	clrwdt			; effacer watchdog
	btfsc	PIE1,TXIE	; message pr�c�dent envoy�?
	goto	$-2		; non, attendre
	btfss	TXSTA,TRMT	; buffer de sortie vide?
	goto	$-4		; non, attendre

			; traiter erreurs
			; ----------------
	movf	flags,w		; charger flags
	andlw	B'00001110'	; conserver flags d'erreur
	btfsc	STATUS,Z	; tester si au moins une erreur
	goto	messprep	; non, traitement normal

	btfss	ER_PAR		; tester si erreur de parit�
	goto	err2		; non, sauter
	PICERR	"P","A","R"," "	; �crire "PAR" dans le buffer de sortie
	bcf	ER_PAR		; acquitter l'erreur
err2
	btfss	ER_FR		; tester si erreur de frame
	goto	err3		; non, sauter
	PICERR	"F","E","R","R"	; �crire "FERR" dans le buffer de sortie
	bcf	ER_FR		; acquitter l'erreur
err3
	btfss	ER_OV		; tester si erreur d'overflow
	goto	msuite		; envoyer le message
	PICERR	"O","E","R","R"	; �crire "OERR" dans le buffer de sortie
	bcf	ER_OV		; acquitter l'erreur
	goto	msuite		; envoyer message d'erreur

		; traitement normal du message
		; ----------------------------
messprep
	bcf	STATUS,RP0	; passer banque 0
	;;TRESQUE
	bcf	INTCON,GIE	; interdire interruptions
 	call	preprep		; pr�parer r�ponse
	bsf	INTCON,GIE	; valider interruptions
	;;TRESQUE
msuite
	bsf	STATUS,RP0	; passer banque 1
	bsf	PIE1,RCIE	; r�autoriser interruption r�ception

			; envoyer r�ponse
			; ---------------
	bsf	PIE1,TXIE	; lancer le message
	goto	loop		; traiter message suivant


	END 			; directive fin de programme