;
; i2c.asm for H4H PROJECT in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Aug 24 01:04:46 2006 Quentin Quadrat
; Last update Tue Sep 12 17:33:31 2006 Quentin Quadrat
;

;*****************************************************************************
;*****************************************************************************
;                              ROUTINES I2C                                  *
;*****************************************************************************
;*****************************************************************************

;-----------------------------------------------------------------------------
; On attend que chaque commande soit termin�e avant de sortir de la
; sous-routine correspondante
;-----------------------------------------------------------------------------
IWAIT	macro	REGISTRE,BIT	; attendre effacement du bit du registre
	clrwdt			; effacer watchdog
	btfsc	REGISTRE,BIT	; bit effac�?
	goto	$-2		; non, attendre
	bcf	STATUS,RP0	; repasser en banque 0
	endm			; fin de macro

;*****************************************************************************
;                        ENVOYER LE START-CONDITION                          *
;*****************************************************************************
i2c_start
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,SEN	; lancer le start-condition
	IWAIT	SSPCON2,SEN	; attendre fin start-condition
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE REPEATED START-CONDITION                 *
;*****************************************************************************
i2c_rstart
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,RSEN	; lancer le repeated start-condition
	IWAIT	SSPCON2,RSEN	; attendre fin repeated start-condition
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE STOP-CONDITION                           *
;*****************************************************************************
i2c_stop
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,PEN	; lancer le stop-condition
	IWAIT	SSPCON2,PEN	; attendre fin stop-condition
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE ACK                                      *
;*****************************************************************************
i2c_ack
	bsf	STATUS,RP0	; passer en banque 1
	bcf	SSPCON2,ACKDT	; le bit qui sera envoy� vaudra " 0 "
	bsf	SSPCON2,ACKEN	; lancer l'acknowledge (= ACKDT = 0 = ACK)
	IWAIT	SSPCON2,ACKEN	; attendre fin ACK
	return			; et retour

;*****************************************************************************
;                        ENVOYER LE NOACK                                    *
;*****************************************************************************
i2c_noack
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,ACKDT	; le bit qui sera envoy� vaudra " 1 "
	bsf	SSPCON2,ACKEN	; lancer l'acknowledge (= ACKDT = 1 = NOACK)
	IWAIT	SSPCON2,ACKEN	; attendre fin NOACK
	return			; et retour

;*****************************************************************************
;                          ENVOYER UN OCTET                                  *
;*****************************************************************************
;-----------------------------------------------------------------------------
; L'octet est pass� dans W
;-----------------------------------------------------------------------------
i2c_write
	movwf	SSPBUF		; lancer l'�mission de l'adresse en mode �criture
	bsf	STATUS,RP0	; passer en banque 1
	IWAIT	SSPSTAT,R_W	; attendre �mission termin�e
	return			; et retour

;*****************************************************************************
;                          LIRE UN OCTET                                     *
;*****************************************************************************
;-----------------------------------------------------------------------------
; L'octet est retourn� dans W
;-----------------------------------------------------------------------------
i2c_read
	bsf	STATUS,RP0	; passer en banque 1
	bsf	SSPCON2,RCEN	; lancer la lecture
	IWAIT	SSPCON2,RCEN	; attendre r�ception termin�e
	movf	SSPBUF,w	; charger octet re�u
	return			; et retour