;
; interruption.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:21:45 2006 Quentin Quadrat
; Last update Tue Sep 12 17:48:29 2006 Quentin Quadrat
;

;*****************************************************************************
;                     ROUTINE INTERRUPTION                                   *
;*****************************************************************************
;-----------------------------------------------------------------------------
; La suppression des lignes "goto restorereg" permet dans ce cas de traiter
; l'interruption sur r�ception et sur �mission en une seule fois
;-----------------------------------------------------------------------------

	;sauvegarder registres
	;---------------------
	org 0x004		; adresse d'interruption
	movwf   w_temp  	; sauver registre W
	swapf	STATUS,w	; swap status avec r�sultat dans w
	movwf	status_temp	; sauver status swapp�
	movf	FSR , w		; charger FSR
	movwf	FSR_temp	; sauvegarder FSR

	; switch vers diff�rentes interrupts
	;-----------------------------------

			; Interruption r�ception USART
			; ----------------------------

	BANKSEL	PIE1		; s�lectionner banque 1
	btfss	PIE1,RCIE	; tester si interrupt autoris�e
	goto 	intsw1		; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR1,RCIF	; oui, tester si interrupt en cours
	goto 	intsw1		; non sauter
	call	intrc		; oui, traiter interrupt
				; LE FLAG NE DOIT PAS ETRE REMIS A 0
;; 	goto 	restorereg

			; Interruption convertisseur A/D
			; -------------------------------
intsw1
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE1,ADIE	;NEW tester si interrupt autoris�e
	goto 	intsw2		;NEW non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR1,ADIF	; oui, tester si interrupt en cours
	goto 	intsw2		; non sauter
	call	intad		; oui, traiter interrupt
	bcf	PIR1,ADIF 	; effacer flag interupt
;; 	goto 	restorereg

			; Interruption Interruption TMR0
			; ------------------------------
intsw2
	btfss	INTCON,T0IE	; tester si interrupt autoris�e
	goto 	intsw3		;NEW non sauter
	btfss	INTCON,T0IF	; oui, tester si interrupt en cours
	goto 	intsw3	; non sauter
	call	inttmr0		; oui, traiter interrupt
	bcf	INTCON,T0IF	; Effacer flag interruption
;; 	goto 	restorereg

			; Interruption Interruption TMR2
			; ------------------------------
intsw3
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE1,TMR2IE	; tester si interrupt autoris�e
	goto 	intsw4		;NEW non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR1,TMR2IF	;NEW oui, tester si interrupt en cours
	goto 	intsw4		; non sauter
	call	inttmr2		; oui, traiter interrupt
	bcf	PIR1,TMR2IF	; Effacer flag interruption
;; 	goto 	restorereg

;; 			; Interruption Interruption RB4 --> RB7
;; 			; -------------------------------------
;;intsw4
;;  	btfss	INTCON,RBIE	; tester si interrupt autoris�e
;;  	goto 	intsw5		;NEW non sauter
;;  	btfss	INTCON,RBIF	; oui, tester si interrupt en cours
;;  	goto 	intsw5		; non sauter
;;  	call	inttrbx		; oui, traiter interrupt
;;  	bcf	INTCON,RBIF	; Effacer flag interruption
;;  	goto 	restorereg

			;NEW Interruption transmission USART
			; -------------------------------
intsw4
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE1,TXIE	; tester si interrupt autoris�e
	goto 	restorereg	; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque 0
	btfss	PIR1,TXIF	; oui, tester si interrupt en cours
	goto 	restorereg	; non sauter
	call	inttx		; oui, traiter interrupt
				; LE FLAG NE DOIT PAS ETRE REMIS A 0

	;restaurer registres
	;-------------------
restorereg
	movf	FSR_temp , w	; charger FSR sauv�
	movwf	FSR		; restaurer FSR
	swapf	status_temp,w	; swap ancien status, r�sultat dans w
	movwf   STATUS		; restaurer status
	swapf   w_temp,f	; Inversion L et H de l'ancien W
                       		; sans modifier Z
	swapf   w_temp,w  	; R�inversion de L et H dans W
				; W restaur� sans modifier status
	retfie  		; return from interrupt
