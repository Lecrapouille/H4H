;
; reponse.asm for hh in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Aug 24 00:54:41 2006 Quentin Quadrat
; Last update Tue Sep 12 19:24:35 2006 Quentin Quadrat
;

;*****************************************************************************
;                      PREPARER LA REPONSE                                   *
;*****************************************************************************
;-----------------------------------------------------------------------------
;TRESQUE r�ponse du PIC au PC : Recuperer les donnees du PIC esclave par I2C.
;
; AUCUNE INTERRUPTIONS POSSIBLES.
;-----------------------------------------------------------------------------
preprep
	clrwdt	; effacer watch dog

	;; Recuperer le contenu du buffer de l'esclave
	;; et envoyer buffer maitre
	;; -------------------------------------------

		; envoyer adresse circuit tant que pas ACK
		; ----------------------------------------
	call	i2c_start	; envoyer start-condition
slave_adress
	movlw	SLAVE_ADDRESS	; charger adresse esclave + �criture
	call	i2c_write	; �crire adresse esclave
	bsf	STATUS,RP0	; passer en banque1
	btfss	SSPCON2,ACKSTAT	; tester ACK re�u
	goto	send_data	; oui, poursuivre OK
	call	i2c_rstart	; non, envoyer repeated start-condition
	goto	slave_adress	; recommencer test

		; envoyer les 2 consignes PWM des moteurs 3 et 4
		; ----------------------------------------------
send_data
	bsf	STATUS,IRP	; pointer banques 2 et 3 en indirect
	movlw	LOW bufin+0x04
	movwf	FSR		; dans pointeur d'adresse
	movlw	D'4'		; 4 octets a envoyer
	movwf	octemp
loop_send
	movf	INDF,w		; charger consigne PWM
	call	i2c_write	; envoyer
	incf	FSR,f		;
	decfsz	octemp,f	; d�cr�menter compteur d'octets
	goto	loop_send	; pas dernier, suivant

	incf	FSR,f
	movf	INDF,octemp1	; sauvegarde retard PIC -- Scilab

		; recevoir les 4 conversions analogiques faites par l'esclave
		; -----------------------------------------------------------
receive_data
	movlw	LOW bufout+0x0B
	movwf	FSR
	movlw	D'8'		; 4 conversions (poids faibles + poid forts)
	movwf	octemp
loop_receive
	call	i2c_read	; lire l'octet
	movwf	INDF		; sauver dans buffer

	decfsz	octemp,f	; d�cr�menter compteur d'octets
	goto	loop_receive	; pas dernier, suivant
	goto	stopi2c		; dernier, trnsmission i2c terminee

stopi2c
		; fin transmission i2c
		; --------------------
	call	i2c_stop	; fin transaction


		;; Dernieres infos a envoyer
		;; -------------------------

			; Retard transmission Scilab -- PIC
			; ---------------------------------
	incf	FSR,f
	movfw	octemp1 ; sauve dans le code qui contient le label 'send_data'

			; Mode Pic
			; --------
 	andlw	B'00000001'	; Mettre le mode 'Scilab'
   	btfsc	PORTB,5		; Si mode 'Scilab' alors
   	iorlw	B'00000010'	; Mettre le mode 'autonome'
 	movwf	INDF		; sauver dans buffer

			; End Of Line
			; -----------
	incf	FSR,f
	movlw	EOL
	movwf	INDF	; sauver dans buffer

	return
