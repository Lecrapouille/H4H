;
; parite.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:20:59 2006 Quentin Quadrat
; Last update Tue Sep 12 17:33:00 2006 Quentin Quadrat
;

;*****************************************************************************
;                        CALCULER LA PARITE                                  *
;*****************************************************************************
;-----------------------------------------------------------------------------
; L'octet dont on calcule la parit� est dans W
; La parit� est paire
; le flag PARITE est positionn� suivant la valeur calcul�e
;-----------------------------------------------------------------------------
calcpar
	movwf	local1		; sauver dans variable locale (temporaire)
	bcf	PARITE		; effacer bit de parit�

calcparl
	andlw	0x01		; garder bit 0 de l'octet
	xorwf	flags,f		; si " 1 ", inverser parit�
	bcf	STATUS,C	; effacer carry pour d�calage
	rrf	local1,f	; amener bit suivant en b0
	movf	local1,w	; charger ce qui reste de l'octet
	btfss	STATUS,Z	; il reste des bits � " 1 " ?
	goto	calcparl	; oui, poursuivre
	return			; non, retour
