;
; timer.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Sat Jul 15 17:58:04 2006 Quentin Quadrat
; Last update Tue Sep 12 18:31:05 2006 Quentin Quadrat
;

;*****************************************************************************
;                     INTERRUPTION TIMER 0                                   *
;*****************************************************************************
; La conversion A/D est relancee a chaque interruption Timer 0
inttmr0
	bsf	ADCON0,GO
	return

;*****************************************************************************
;                     INTERRUPTION TIMER 2                                   *
;*****************************************************************************
; Fin des cycles PWM generes par les pattes CCP1 et CCP2.
; Changement du rapport cyclique du prochain signal PWM (rapport entre le temps
; de l'etat haut et le temps de l'etat bas).
;
inttmr2
	clrwdt	; effacer watch dog

	; Calcul PWM du CCP1
	; ==================
			; Copier SCICOSPWM1H dans zonevalH
			; --------------------------------
	BANKSEL bufin
	movf	SCICOSPWM1H,w
	BANK0
	movwf	zonevalH

			; Copier SCICOSPWM1L dans zonevalL
			; --------------------------------
	BANKSEL bufin
	movf	SCICOSPWM1L,w
	BANK0
	movwf	zonevalL

			; initialiser bits fractionnaires
			; -------------------------------
	bcf	CCP1CON,CCP1Y	; effacer bit 0
	bcf	CCP1CON,CCP1X	; effacer bit 1

			; Copier CCP1CON dans ccpcon
			; --------------------------
	movf	CCP1CON,w
	movwf	ccpcon
	call	calculpulse	; Calculer la nouvelle pulsation PWM

			; placer nouvelle valeur de consigne
			; ----------------------------------
	movwf	CCPR1L		; resultat est dans W
				; copier resultat dans CCPR1L
	movf	ccpcon,w	; Restaurer CCP1CON
	movwf	CCP1CON		;


	; Calcul PWM du CCP2
	; ==================
			; Copier SCICOSPWM2H dans zonevalH
			; ----------------------------------
	BANKSEL bufin
	movf	SCICOSPWM2H,w
	BANK0
	movwf	zonevalH


			; Copier SCICOSPWM2H dans zonevalL
			; ----------------------------------
	BANKSEL bufin
	movf	SCICOSPWM2L,w
	BANK0
	movwf	zonevalL

			; initialiser bits fractionnaires
			; -------------------------------
	bcf	CCP2CON,CCP2Y	; effacer bit 0
	bcf	CCP2CON,CCP2X	; effacer bit 1

			; Copier CCP2CON dans ccpcon
			; --------------------------
	movf	CCP2CON,w
	movwf	ccpcon
	call	calculpulse	; Calculer la nouvelle pulsation PWM

			; placer nouvelle valeur de consigne
			; ----------------------------------
	movwf	CCPR2L
	movf	ccpcon,w
	movwf	CCP2CON

	return

calculpulse
		; OFFSET + zoneval : addition 10 bits
		; ===================================
			; Poids faible
			; ------------
	movlw	OFFSETL		; Mettre la val de l'Offset de poids faible
	addwf	zonevalL,f	; dans W et l'additionner a la valeur analogique
				; de poids faible

			; Poids fort
			; ----------
	btfsc	STATUS,C	; Additioner le Carry
	incf	zonevalH,f	; a la valeur analogique de poids fort

	movlw	OFFSETH		; Mettre la val de l'Offset de poids fort
	addwf	zonevalH,f	; dans W et l'additionner a la valeur analogique
				; de poids fort

	;; MAINTENANT AVEC OFFSET == 0x00 on a plus besoin de la /2
			; division sur 10 bits de 'zoneval' par 2
			; ---------------------------------------
	;; 	bcf 	STATUS,C	; Carry := 0
	;; 	rrf 	zonevalH,f  	; zonevalH := zonevalH / 2 + carry * 2^7
	;; 	rrf 	zonevalL,f	; zonevalL := zonevalL / 2 + carry * 2^7

	;; SI OFFSET == 0x03 0xFF alors decommenter le code /2

		; Placer valeur entiere sur 8 bits (div par 4)
		; ============================================

			; division par 2
			; --------------
	bcf 	STATUS,C	; Carry := 0
	rrf 	zonevalH,f  	; zonevalH := zonevalH / 2 + carry * 2^7
	rrf 	zonevalL,f	; zonevalL := zonevalL / 2 + carry * 2^7

			; sauver le premier bit fractionnaire
			; -----------------------------------
	btfsc	STATUS,C	; tester si futur bit "-1" = 1
	bsf	ccpcon,5	; oui, mettre bit 1 � 1

			; division par 2
			; --------------
	bcf 	STATUS,C	; Carry := 0
	rrf 	zonevalH,f  	; zonevalH := zonevalH / 2 + carry * 2^7
	rrf 	zonevalL,w	; w := zonevalL / 2 + carry * 2^7

			; sauver le premier bit fractionnaire
			; -----------------------------------
	btfsc	STATUS,C 	; tester si futur bit "-2" = 1
	bsf	ccpcon,4	; oui, mettre bit 0 � 1

	return
