;
; emission.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:23:57 2006 Quentin Quadrat
; Last update Tue Sep 12 17:33:40 2006 Quentin Quadrat
;

;*****************************************************************************
;                     INTERRUPTION EMISSION USART                            *
;*****************************************************************************
;-----------------------------------------------------------------------------
; envoie le caract�re point� par bufoutptr, puis incr�mente le pointeur
; Si ce caract�re est le "line-feed", alors on arr�te l'�mission et on pointe de
; nouveau au d�but du buffer
;-----------------------------------------------------------------------------
inttx
			; prendre octet � envoyer
			; -----------------------

	bsf	STATUS,IRP	; pointer banques 2 et 3 en indirect
	movf	bufoutptr,w	; charger pointeur d'octets
	movwf	FSR		; dans pointeur
	movf	INDF,w		; charger octet � envoyer

			; calculer parit�
			; ---------------
	call	calcpar		; calculer parit�
	bsf	STATUS,RP0	; passer banque1
	bcf	TXSTA,TX9D	; par d�faut, parit� = 0
	btfsc	PARITE		; parit� = 1?
	bsf	TXSTA,TX9D	; oui, positionner parit�

			; envoyer octet
			; -------------
	movf	INDF,w		; charger octet � envoyer
	bcf	STATUS,RP0	; passer banque 0
	movwf	TXREG		; envoyer octet + parit�
	incf	bufoutptr,f	; pointer sur octet suivant

			; tester si dernier octet
			; -----------------------
	xorlw	EOL		; comparer octet envoy� avec Line-feed
	btfss	STATUS,Z	; �galit�?
	return			; non, retour d'interruption

			; traiter fin d'�mission
			; ----------------------
	bsf	STATUS,RP0	; passer en banque 1
	bcf	PIE1,TXIE	; fin des interruptions �mission USART
	bcf	STATUS,RP0	; repasser banque 0
	movlw	LOW bufout	; adresse du buffer de sortie
	movwf	bufoutptr	; prochain caract�re = premier
	return			; fin d'interruption