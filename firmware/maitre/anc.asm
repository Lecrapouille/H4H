;
; anc.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Sat Jul 15 17:54:01 2006 Quentin Quadrat
; Last update Tue Sep 12 19:08:53 2006 Quentin Quadrat
;

;*****************************************************************************
;                     INTERRUPTION CONVERTISSEUR A/D                         *
;*****************************************************************************
;-----------------------------------------------------------------------------
; Copier le resultat de la conversion analogique dans le buffer de sortie du
; port serie et change de patte analogique. Il faut attendre que les condensateurs
; se chargent avant de lancer une nouvelle conversion (utilisation Timer 0).
;-----------------------------------------------------------------------------
intad
	clrwdt	; effacer watch dog

			; Lire dans ADCON0, le numero de la derniere
			; patte analogique utilisee et multiplier par
			; 2 le resultat. Resultat final est dans octemp.
			; ----------------------------------------------
	clrf	octemp
	btfsc	ADCON0,3
	bsf	octemp,1 ; decalage de 1 sur la gauche <==> multiplication par 2
	btfsc	ADCON0,4
	bsf	octemp,2
	btfsc	ADCON0,5
	bsf	octemp,3

			; Changer de pattes analogique (ADCON0)
			; -------------------------------------
	btfss	ADCON0,5	; Tester si patte == RA5/AN4
	goto	intad00
	goto	intad01
intad00
	movlw	B'00001000'	; mask permettant passer a la patte analog suivante
	addwf	ADCON0,f	; ADCON0 := ADCON0 + B'00001000' (le b0 est deja a 1)
	goto	intadend
intad01
	movlw	B'10000001'	; mask permettant de cycler sur patte RA0/AN0
	movwf	ADCON0		; ADCON0 := B'10000001'
intadend

			; Remplissage buffer correspond a la patte precedente
			; ---------------------------------------------------
	bsf	STATUS,IRP	; pointer banques 2 et 3 en indirect
	movlw	LOW bufout
	addwf	octemp,w	; charger pointeur bufout + analog Channel Select
	movwf	FSR		; dans pointeur d'adresse


			; Sauvegarde des bits de poids forts dans buffout
			; ------------------------------------------------
	movf	ADRESH,w	; copier result analog dans W (poids fort)
	movwf	INDF		; sauver dans buffer
	incf	FSR,f		; pointeur boufout += 1

			; Sauvegarde des bits de poids faibles dans buffout
			; Attention la valeur 0xFF est reservee pour EOL.
			; ------------------------------------------------
	movlw	EOL		; Copier EOL dans reg temp pour
	movwf	octemp1		; gestion du conflit ADRESL == EOL
	BANK1
	movf	ADRESL,w	; copier result analog dans W (poids faible)
	BANK0
	xorwf	octemp1,f	; Tester si ADRESL == EOL. Resultat est dans Z
	btfsc	STATUS,Z	; tester si w == EOL (valeur reservee).
	movlw	EOL-0x01	; Si ADRESL == EOL alors w := ADRESL - 1
	movwf	INDF		; Pas de probleme ADRESL != EOL donc pas de conflit

	return
