;
; mainloop.asm<2> for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Tue Jul 18 12:19:25 2006 Quentin Quadrat
; Last update Tue Sep 12 17:33:12 2006 Quentin Quadrat
;

;*****************************************************************************
;                      PROGRAMME PRINCIPAL                                   *
;*****************************************************************************

start
;;TRESQUE
;; 		; lancer l'�mission, message de bienvenue
;; 		; ---------------------------------------

;; 	bsf	STATUS,RP0	; passer en banque 1
;; 	bsf	TXSTA,TXEN	; �mission en service.
;; 	bsf	PIE1,TXIE	; envoyer message "pret"


;; 			; attendre fin de l'�mission
;; 			; --------------------------
;; 	clrwdt			; effacer watch dog
;; 	btfsc	PIE1,TXIE	; reste des caract�res � envoyer?
;; 	goto	$-2		; oui, attendre
;; 	btfss	TXSTA,TRMT	; buffer de sortie vide?
;; 	goto	$-4		; non, attendre

			; lancer la r�ception
			; --------------------
	bcf	STATUS,RP0	; passer banque 0
	bsf	RCSTA,CREN	; lancer la r�ception

			; revevoir le message
			; -------------------
	bsf	STATUS,RP0	; passer banque 1
loop
	bsf	PIE1,RCIE	; autoriser interruption r�ception
	clrwdt			; effacer watchdog
	btfsc	PIE1,RCIE	; tester si message complet re�u
	goto	$-2		; non, attendre


;;TRESQUE		; Recuperer les donnees du PIC esclave par SPI
;; 			; --------------------------------------------
;; 	;; 	call	datafromslave
;; 	BANK2
;; 	movf	bufin+0x04,w
;; 	BANK3
;; 	movwf	bufout+0x0A
;; 	BANK1

		; Attendre fin �mission pr�c�dente
		; --------------------------------
	clrwdt			; effacer watchdog
	btfsc	PIE1,TXIE	; message pr�c�dent envoy�?
	goto	$-2		; non, attendre
	btfss	TXSTA,TRMT	; buffer de sortie vide?
	goto	$-4		; non, attendre

			; traiter erreurs
			; ----------------
	movf	flags,w		; charger flags
	andlw	B'00001110'	; conserver flags d'erreur
	btfsc	STATUS,Z	; tester si au moins une erreur
	goto	messprep	; non, traitement normal

	btfss	ER_PAR		; tester si erreur de parit�
	goto	err2		; non, sauter
	PICERR	"P","A","R"," "	; �crire "PAR" dans le buffer de sortie
	bcf	ER_PAR		; acquitter l'erreur
err2
	btfss	ER_FR		; tester si erreur de frame
	goto	err3		; non, sauter
	PICERR	"F","E","R","R"	; �crire "FERR" dans le buffer de sortie
	bcf	ER_FR		; acquitter l'erreur
err3
	btfss	ER_OV		; tester si erreur d'overflow
	goto	msuite		; envoyer le message
	PICERR	"O","E","R","R"	; �crire "OERR" dans le buffer de sortie
	bcf	ER_OV		; acquitter l'erreur
	goto	msuite		; envoyer message d'erreur

		; traitement normal du message
		; ----------------------------
messprep
	bcf	STATUS,RP0	; passer banque 0
	;;TRESQUE
	bcf	INTCON,GIE	; interdire interruptions
 	call	preprep		; pr�parer r�ponse
	bsf	INTCON,GIE	; valider interruptions
	;;TRESQUE
msuite
	bsf	STATUS,RP0	; passer banque 1
	bsf	PIE1,RCIE	; r�autoriser interruption r�ception

			; envoyer r�ponse
			; ---------------
	bsf	PIE1,TXIE	; lancer le message
	goto	loop		; traiter message suivant
