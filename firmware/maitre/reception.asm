;
; reception.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:22:45 2006 Quentin Quadrat
; Last update Tue Sep 12 19:27:31 2006 Quentin Quadrat
;

;*****************************************************************************
;                     INTERRUPTION RECEPTION USART                           *
;*****************************************************************************
;-----------------------------------------------------------------------------
; Re�oit le caract�re de l'USART et le place dans le buffer d'entr�e.
; Si la longueur atteint D'94', on n'encode plus, et on place 0x0D en avant-
; derni�re position et EOL en derni�re position
; Si la r�ception est termin�e (longueur atteinte ou EOL re�u), on stoppe les
; interruptions de r�ception et on repositionne le pointeur au d�but du buffer
; Les erreurs sont d�tect�es et signal�es
;-----------------------------------------------------------------------------
intrc
			; tester si erreur de frame
			; -------------------------
	btfsc	RCSTA,FERR	; tester si erreur de frame
	bsf	ER_FR		; oui, signaler erreur de frame

			; lire la parit�
			; --------------
	bcf	PARITE		; par d�faut, parit� = 0
	btfsc	RCSTA,RX9D	; parit� lue = 1?
	bsf	PARITE		; oui, le signaler

			; lire octet re�u
			; ---------------
	bsf	STATUS,IRP	; pointer banques 2 et 3 en indirect
	movf	bufinptr,w	; charger pointeur destination
	movwf	FSR		; dans pointeur d'adresse
	movf	RCREG,w		; charger octet re�u
	movwf	INDF		; sauver dans buffer

			; v�rifier la parit�
			; ------------------
intrc1
	movf	INDF,w		; charger caract�re re�u
	call	calcpar		; calculer la parit�
	movf	RCSTA,w		; charger registre commande
	xorwf	flags,w		; comparer parit� re�ue et calcul�e
	andlw	0x01		; ne garder que le r�sultat
	btfss	STATUS,Z	; b0 = b1? (parit� calcul�e = parit� re�ue?)
	bsf	ER_PAR		; non, signaler erreur de parit�

			; tester si erreur d'overflow
			; ---------------------------
	btfsc	PIR1,RCIF	; encore d'autres octets dans RCREG?
	goto	intrc2		; oui, v�rifier caract�re
	btfss	RCSTA,OERR	; non, erreur d'overflow?
	goto	intrc2		; non, v�rifier caract�re
	bcf	RCSTA,CREN	; oui, arr�t de la r�ception (reset de OERR)
	bsf	RCSTA,CREN	; remise en service de la r�ception
	bsf	ER_OV		; signaler erreur overflow
	goto	intrcend	; et fin d'interruption

			; tester si caract�re re�u == EOL
			; -------------------------------
intrc2
	movf	INDF,w		; charger caract�re re�u
	xorlw	EOL		; comparer avec line-feed
	btfsc	STATUS,Z	; identique?
	goto	intrcend	; oui, fin de message

			; v�rifier si buffer plein
			; ------------------------
	incf	bufinptr,f	; incr�menter pointeur de caract�res
	movf	FSR,w		; charger pointeur
	xorlw	0x6D		; comparer avec derni�r emplacement possible
	btfss	STATUS,Z	; identique?
	return			; non, fin de r�ception
	incf	FSR,f		; pointeur sur emplacement suivant
	movlw	EOL		; charger line-feed
	movwf	INDF		; en derni�re position

			; fin de message
			; --------------
intrcend
	movlw	LOW bufin	; oui, adresse de d�part du buffer d'entr�e
	movwf	bufinptr	; prochain caract�re sera le premier
	bsf	STATUS,RP0	; passer banque 1
	bcf	PIE1,RCIE	; fin des interruptions de r�ception
	bcf	STATUS,RP0	; repasser banque 0
	return			; et retour