;
; macros.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:03:51 2006 Quentin Quadrat
; Last update Wed Sep 13 15:04:20 2006 Quentin Quadrat
;

;*****************************************************************************
;                               ASSIGNATIONS SYSTEME                         *
;*****************************************************************************

		; REGISTRE OPTION_REG (configuration)
		; -----------------------------------
OPTIONVAL	EQU	B'10001111'
			; RBPU     b7 : 1= R�sistance rappel +5V hors service
			; PSA      b3 : 1= Assignation pr�diviseur sur Watchdog
			; PS2/PS0  b2/b0 valeur du pr�diviseur = 256

		; REGISTRE INTCON (contr�le interruptions standard)
		; -------------------------------------------------
INTCONVAL	EQU	B'01100000'
			; GIE	b7 : activation generale interruptions
			; PEIE	b6 : masque autorisation g�n�rale p�riph�riques
			; T0IE	b5 : Timer 0
			; RBIE	b3 : Int chgt de niv RB4 --> RB7

		; REGISTRE PIE1 (contr�le interruptions p�riph�riques)
		; ----------------------------------------------------
PIE1VAL		EQU	B'01000010'
			; ADIE      b6 : masque interrupt convertisseur A/D
			; TMR2IE    b1 : masque interrupt TMR2 = PR2

		; REGISTRE ADCON1 (ANALOGIQUE/DIGITAL)
		; ------------------------------------
ADCON1VAL	EQU	B'10000000'
			; RA0/AN0 --> RA5/AN4 (sauf RA4) en analogique
		    	; justification � droite

SLAVE_ADDRESS	EQU	0x30

		; MACROS UTILISATEUR buffin et buffout
		; ------------------------------------
define([SCICOSPWM1H],[bufin+0x00]) ; Reponse Scilab: nouvelle consigne PWM du moteur 1 (High)
define([SCICOSPWM1L],[bufin+0x01]) ; Reponse Scilab: nouvelle consigne PWM du moteur 1 (Low)
define([SCICOSPWM2H],[bufin+0x02]) ; Reponse Scilab: nouvelle consigne PWM du moteur 2 (High)
define([SCICOSPWM2L],[bufin+0x03]) ; Reponse Scilab: nouvelle consigne PWM du moteur 2 (Low)

define([SCICOSPWM3H],[bufin+0x04]) ; Reponse Scilab: nouvelle consigne PWM du moteur 3 (High)
define([SCICOSPWM3L],[bufin+0x05]) ; Reponse Scilab: nouvelle consigne PWM du moteur 3 (Low)
define([SCICOSPWM4H],[bufin+0x06]) ; Reponse Scilab: nouvelle consigne PWM du moteur 4 (High)
define([SCICOSPWM4L],[bufin+0x07]) ; Reponse Scilab: nouvelle consigne PWM du moteur 4 (Low)

 		; caractere de terminaison d'un message pour le port serie
 		; --------------------------------------------------------
EOL	EQU	0xFF

;*****************************************************************************
;                           ASSIGNATIONS PROGRAMME                           *
;*****************************************************************************
BRGVAL	EQU	D'64'	; D'129' pour un d�bit de 9615 bauds en mode high-speed
			; D'20'  pour un d�bit de 56.7 Kbauds en mode high-speed
			; D'64'  pour un d�bit de 19.2 Kbauds en mode high-speed

OFFSETL	 EQU 	0x00		; valeur mini de CCPRx en quarts de valeurs
OFFSETH	 EQU 	0x00		; valeur mini de CCPRx en quarts de valeurs
				; OFFSET = OFFSETH::OFFSETL = 1023

	;; TIMER 2 pour generation signal PWM a 5kHz
	;; -----------------------------------------
PR2VAL	 EQU	0xFF	; valeur d'initialisation de PR2 (a muliplier par prediciseur)
			; Nous, dans ce projet on veut que le timer deborde a la valeur
			; de 1023 (0xFF * prediv 4).


;*****************************************************************************
;                             MACRO                                          *
;*****************************************************************************
;-----------------------------------------------------------------------------
; Message d'erreur a envoyer au PC. Protocole de communication
; b7 du premier octet == 0 : pas d'erreur
; b7 du premier octet == 1 : erreur
;-----------------------------------------------------------------------------
PICERR	macro	a1,a2,a3,a4	; inscrit le message dans bufout
	BANKSEL	bufout		; passer en banque 3
	movlw	B'10000000'	; Indicateur d'erreur pour le PC
	movwf	bufout		; dans bufout
	movlw	a1		; charger argument 1
	movwf	bufout+0x01	; dans bufout
	movlw	a2		; charger argument 2
	movwf	bufout+0x02	; dans bufout
	movlw	a3		; charger argument 3
	movwf	bufout+0x03	; dans bufout
	movlw	a4		; charger argument 4
	movwf	bufout+0x04	; dans bufout
	movlw	EOL		; charger line-feed
	movwf	bufout+0x11	; dans bufout
	BANKSEL	0		; repasser banque 0
	endm

			; Changement de banques
			; ----------------------

BANK0	macro				; passer en banque0
		bcf	STATUS,RP0
		bcf	STATUS,RP1
	endm

BANK1	macro				; passer en banque1
		bsf	STATUS,RP0
		bcf	STATUS,RP1
	endm

BANK2	macro				; passer en banque2
		bcf	STATUS,RP0
		bsf	STATUS,RP1
	endm

BANK3	macro				; passer en banque3
		bsf	STATUS,RP0
		bsf	STATUS,RP1
	endm
