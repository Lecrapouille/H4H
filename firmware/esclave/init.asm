;
; init.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Tue Sep 12 17:26:19 2006 Quentin Quadrat
; Last update Wed Sep 13 15:58:19 2006 Quentin Quadrat
;

;*****************************************************************************
;                          INITIALISATIONS                                   *
;*****************************************************************************

init
			; Registre d'options (banque 1)
			; -----------------------------
	BANK1			; s�lectionner banque 1
	movlw	OPTIONVAL	; charger masque
	movwf	OPTION_REG	; initialiser registre option

			; TRISx
			; -----
	bcf	TRISC,2		; CCP1/RC2 en sortie
	bcf	TRISC,1		; CCP2/RC1 en sortie

			; initialiser I�C (banque 1)
			; -------------------------
	;; FIXME: mettre address i�c maitre mettre trame serie

	movlw	SLAVE_ADDRESS	; valeur de l'adresse PIC esclave
	movwf	SSPADD		; dans registre d'adresse
	clrf	SSPSTAT		; slew rate control ON, compatible I�C
;;(facultatif)	movlw	B'10000000'	; appel broadcast
;; 	movlw	SSPCON2		;
	bcf	STATUS,RP0	; passer banque 0
	movlw	B'00110110'	; module MSSP en service, mode I�C esclave 7 bits, SCL ok
	movwf	SSPCON		; dans registre de contr�le
	BANK1

			; registres interruptions (banque 1)
			; ----------------------------------
	movlw	INTCONVAL	; charger valeur registre interruption
	movwf	INTCON		; initialiser interruptions
	movlw	PIE1VAL		; Initialiser registre
	movwf	PIE1		; interruptions p�riph�riques 1
	movlw	PIE2VAL		; Initialiser registre
	movwf	PIE2		; interruptions p�riph�riques 2

 			; Initialise les buffers
 			; ----------------------
	BANKSEL	bufin
 	movlw	0x00		; consignes PWM des 4 moteurs := 0
 	movwf	bufin+0x00	; moteur 1 (poids fort)
 	movwf	bufin+0x01	; moteur 1 (poids faible)
 	movwf	bufin+0x02	; moteur 2 (poids fort)
 	movwf	bufin+0x03	; moteur 2 (poids faible)
 	movwf	bufin+0x04	; moteur 3 (poids fort)
 	movwf	bufin+0x05	; moteur 3 (poids faible)
 	movwf	bufin+0x06	; moteur 4 (poids fort)
 	movwf	bufin+0x07	; moteur 4 (poids faible)

	BANKSEL	bufout
 	movlw	0x00		; 4 conversions analogiques (poids fort + faibles)
 	movwf	bufout+0x00	; dans bufout (==> buffer vide)
 	movwf	bufout+0x01	; dans bufout (==> buffer vide)
 	movwf	bufout+0x02	; dans bufout (==> buffer vide)
 	movwf	bufout+0x03	; dans bufout (==> buffer vide)
 	movwf	bufout+0x04	; dans bufout (==> buffer vide)
 	movwf	bufout+0x05	; dans bufout (==> buffer vide)
 	movwf	bufout+0x06	; dans bufout (==> buffer vide)
 	movwf	bufout+0x07	; dans bufout (==> buffer vide)
 	movwf	bufout+0x08	; dans bufout (==> buffer vide)
 	movwf	bufout+0x09	; dans bufout (==> buffer vide)
 	movwf	bufout+0x0A	; dans bufout (==> buffer vide)
	BANK0

			; initialiser variables
			; ---------------------
 	movlw	LOW bufin	; adresse du buffer de r�ception
 	movwf	bufinptr	; dans pointeur
 	movlw	LOW bufout	; adresse basse du buffer d'�mission
 	movwf	bufoutptr	; dans pointeur


			; configurer le module CCP1 et CCP2 et timer2
			; -------------------------------------------
	movlw	B'00001100'	; pour mode PWM
	movwf	CCP1CON		; dans registre de commande CCP1
	movwf	CCP2CON		; dans registre de commande CCP2
	movlw	PR2VAL		; valeur de d�bordement
	bsf	STATUS,RP0	; passer en banque 1
	movwf	PR2		; dans registre de comparaison Timer 2
	bcf	STATUS,RP0	; repasser en banque 0
	movlw	B'00000101'	; timer 2 on, pr�diviseur = 4
	movwf	T2CON		; dans registre de contr�le
				; LES SIGNAUX PWM SONT GENERES A 5kHz

			; configurer le convertisseur A/D
			; -------------------------------
	movlw	ADCON1VAL	; 1 entr�e analogique
	bsf	STATUS,RP0	; passer banque 1
	movwf	ADCON1		; �criture dans contr�le1 A/D
	bcf	STATUS,RP0	; repasser banque 0
	movlw	B'10000001'	; convertisseur ON, pr�diviseur 32
	movwf	ADCON0		; dans registre de contr�le 0

			; autoriser interruptions (banque 0)
			; ----------------------------------
	clrf	PIR1		; effacer flags 1 interruption
	clrf	PIR2		; effacer flags 2 interruption
	bsf	INTCON,GIE	; valider interruptions
	goto	start		; programme principal