;
; macros.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:03:51 2006 Quentin Quadrat
; Last update Wed Sep 13 15:52:51 2006 Quentin Quadrat
;

;*****************************************************************************
;                               ASSIGNATIONS SYSTEME                         *
;*****************************************************************************

		; REGISTRE OPTION_REG (configuration)
		; -----------------------------------
OPTIONVAL	EQU	B'10001111'
			; RBPU     b7 : 1= R�sistance rappel +5V hors service
			; PSA      b3 : 1= Assignation pr�diviseur sur Watchdog
			; PS2/PS0  b2/b0 valeur du pr�diviseur = 256

		; REGISTRE INTCON (contr�le interruptions standard)
		; -------------------------------------------------
INTCONVAL	EQU	B'01100000'
			; GIE	b7 : activation generale interruptions
			; PEIE	b6 : masque autorisation g�n�rale p�riph�riques
			; T0IE	b5 : Timer 0
			; RBIE	b3 : Int chgt de niv RB4 --> RB7

		; REGISTRE PIE1 (contr�le interruptions p�riph�riques)
		; ----------------------------------------------------
PIE1VAL		EQU	B'01000010'
			; ADIE      b6 : masque interrupt convertisseur A/D
			; TMR2IE    b1 : masque interrupt TMR2 = PR2

		; REGISTRE PIE2 (contr�le interruptions p�riph�riques)
		; ----------------------------------------------------
PIE2VAL		EQU	B'00001000'
			; BCLIE    b3 : masque interrupt SSP mode I2C

		; REGISTRE ADCON1 (ANALOGIQUE/DIGITAL)
		; ------------------------------------
ADCON1VAL	EQU	B'10000000'
			; RA0/AN0 --> RA5/AN4 (sauf RA4) en analogique
		    	; justification � droite

;*****************************************************************************
;                           ASSIGNATIONS PROGRAMME                           *
;*****************************************************************************
OFFSETL	 EQU 	0x00		; valeur mini de CCPRx en quarts de valeurs
OFFSETH	 EQU 	0x00		; valeur mini de CCPRx en quarts de valeurs
				; OFFSET = OFFSETH::OFFSETL = 1023

	;; TIMER 2 pour generation signal PWM a 5kHz
	;; -----------------------------------------
PR2VAL	 EQU	0xFF	; valeur d'initialisation de PR2 (a muliplier par prediciseur)
			; Nous, dans ce projet on veut que le timer deborde a la valeur
			; de 1023 (0xFF * prediv 4).

SLAVE_ADDRESS	EQU	0x30 	; adresse i2c esclave

;*****************************************************************************
;                             MACRO                                          *
;*****************************************************************************

			; Changement de banques
			; ----------------------

BANK0	macro				; passer en banque0
		bcf	STATUS,RP0
		bcf	STATUS,RP1
	endm

BANK1	macro				; passer en banque1
		bsf	STATUS,RP0
		bcf	STATUS,RP1
	endm

BANK2	macro				; passer en banque2
		bcf	STATUS,RP0
		bsf	STATUS,RP1
	endm

BANK3	macro				; passer en banque3
		bsf	STATUS,RP0
		bsf	STATUS,RP1
	endm
