;
; variables.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:10:42 2006 Quentin Quadrat
; Last update Wed Sep 13 11:45:40 2006 Quentin Quadrat
;

;*****************************************************************************
;                        VARIABLES BANQUE 0                                  *
;*****************************************************************************

; Zone de 80 bytes
; ----------------

	CBLOCK	0x20		; D�but de la zone (0x20 � 0x6F)
	octemp : 1		; sauvegarde temporaire
	octemp1 : 1		; sauvegarde temporaire
 	zonevalH : 1		; copie temp valeur SCICOSPWMxH (poids fort)
 	zonevalL : 1		; copie temp valeur SCICOSPWMxL (poids faible)
 	ccpcon:	1		; copie temp de CCP1CON ou CCP2CON
        ENDC			; Fin de la zone

;*****************************************************************************
;                      VARIABLES ZONE COMMUNE                                *
;*****************************************************************************

; Zone de 16 bytes
; ----------------

	CBLOCK 0x70		; D�but de la zone (0x70 � 0x7F)
	w_temp : 1		; Sauvegarde registre W
	status_temp : 1		; sauvegarde registre STATUS
	FSR_temp : 1		; sauvegarde FSR (si indirect en interrupt)
	PCLATH_temp : 1		; sauvegarde PCLATH (si prog>2K)
	bufinptr : 1		; pointeur sur caract�re courant buffer entr�e
	bufoutptr : 1		; pointeur sur caract�re courant buffer sortie
	ENDC

;*****************************************************************************
;                        VARIABLES BANQUE 2                                  *
;*****************************************************************************

; Zone de 96 bytes
; ----------------

	CBLOCK	0x110		; D�but de la zone (0x110 � 0x16F)
	bufin : D'96'		; zone de stockage des donn�es entr�es (I2C)

	ENDC			; Fin de la zone

;*****************************************************************************
;                        VARIABLES BANQUE 3                                  *
;*****************************************************************************

; Zone de 96 bytes
; ----------------

	CBLOCK	0x190		; D�but de la zone (0x190 � 0x1EF)
	bufout : D'96'		; message � envoyer (I2C)
	ENDC			; Fin de la zone
