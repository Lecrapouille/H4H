;
; interruption.asm for H4H in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Jun 29 18:21:45 2006 Quentin Quadrat
; Last update Wed Sep 13 15:08:23 2006 Quentin Quadrat
;

;*****************************************************************************
;                     ROUTINE INTERRUPTION                                   *
;*****************************************************************************
;-----------------------------------------------------------------------------
; La suppression des lignes "goto restorereg" permet dans ce cas de traiter
; l'interruption sur r�ception et sur �mission en une seule fois
;-----------------------------------------------------------------------------

	;sauvegarder registres
	;---------------------
	org 0x004		; adresse d'interruption
	movwf   w_temp  	; sauver registre W
	swapf	STATUS,w	; swap status avec r�sultat dans w
	movwf	status_temp	; sauver status swapp�
	movf	FSR , w		; charger FSR
	movwf	FSR_temp	; sauvegarder FSR

	; switch vers diff�rentes interrupts
	;-----------------------------------

			; Interruption I2C
			; ----------------
intsw0
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE2,BCLIE	; tester si interrupt autoris�e
	goto 	intsw1		; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR2,BCLIF	; oui, tester si interrupt en cours
	goto 	intsw1		; non sauter
	call	intssp		; oui, traiter interrupt
	bcf	PIR2,BCLIF	; Effacer flag interruption

			; Interruption convertisseur A/D
			; -------------------------------
intsw1
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE1,ADIE	; tester si interrupt autoris�e
	goto 	intsw2		; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR1,ADIF	; oui, tester si interrupt en cours
	goto 	intsw2		; non sauter
	call	intad		; oui, traiter interrupt
	bcf	PIR1,ADIF 	; effacer flag interupt

			; Interruption Interruption TMR0
			; ------------------------------
intsw2
	btfss	INTCON,T0IE	; tester si interrupt autoris�e
	goto 	intsw3		; non sauter
	btfss	INTCON,T0IF	; oui, tester si interrupt en cours
	goto 	intsw3		; non sauter
	call	inttmr0		; oui, traiter interrupt
	bcf	INTCON,T0IF	; Effacer flag interruption

			; Interruption Interruption TMR2
			; ------------------------------
intsw3
	bsf	STATUS,RP0	; s�lectionner banque1
	btfss	PIE1,TMR2IE	; tester si interrupt autoris�e
	goto 	restorereg	; non sauter
	bcf	STATUS,RP0	; oui, s�lectionner banque0
	btfss	PIR1,TMR2IF	; oui, tester si interrupt en cours
	goto 	restorereg	; non sauter
	call	inttmr2		; oui, traiter interrupt
	bcf	PIR1,TMR2IF	; Effacer flag interruption


			; Restaurer registres
			;-------------------
restorereg
	movf	FSR_temp , w	; charger FSR sauv�
	movwf	FSR		; restaurer FSR
	swapf	status_temp,w	; swap ancien status, r�sultat dans w
	movwf   STATUS		; restaurer status
	swapf   w_temp,f	; Inversion L et H de l'ancien W
                       		; sans modifier Z
	swapf   w_temp,w  	; R�inversion de L et H dans W
				; W restaur� sans modifier status
	retfie  		; return from interrupt
