;
; reponse.asm for hh in /Users/crapouille
;
; Ce programme s'aide des exemples du cours de Bigonoff (http://www.bigonoff.org/)
;
; Started on  Thu Aug 24 00:54:41 2006 Quentin Quadrat
; Last update Wed Sep 13 15:20:10 2006 Quentin Quadrat
;

;*****************************************************************************
;                      PREPARER LA REPONSE                                   *
;*****************************************************************************

;*****************************************************************************
;			INTERRUPTION SSP en mode I2C
;*****************************************************************************
;-----------------------------------------------------------------------------
; Gestion de l'interruption en mode esclave.
; cas 1 : reception de l'adresse en mode ecriture
; cas 2 : reception d'une donnee
; cas 3 : reception de l'adresse en mode lecture
; cas 4 : envoi d'une donnee en mode lecture
; cas 5 : reception du NOACK de fin de lecture
; erreur : tout autre cas est incorrect, on provoque le reset du PIC par
; debordement du watchdog (a vous de traiter de facon appropriee)
;-----------------------------------------------------------------------------
intssp
			; conserver les bits utiles
			; --------------------------
	BANKSEL	SSPSTAT 	; passer en banque 1
	movf	SSPSTAT,w 	; charger valeur SSPSTAT
	andlw	B'00101101' 	; garder D_A, S, R_W, et BF
	bcf	STATUS,RP0	; repasser en banque 0
	movwf	ssptemp		; sauvegarder dans variable temporaire

		; cas 1 : reception adresse ecriture
		; ----------------------------------
	xorlw	B'00001001'	; buffer plein, contient une adresse
	btfss	STATUS,Z	; condition realisee ?
	goto	intssp2		; non, examiner cas 2
	movlw	LOW bufin	; charger adresse du buffer de reception
	movwf	ptrin		; le prochain octet sera le premier du buffer in
;; 	bcf	flaggen		; par defaut, pas une commande generale (broadcast)
	movf	SSPBUF,w	; efface BF, teste l'adresse
;; 	btfsc	STATUS,Z	; adresse generale recue ?
;; 	bsf	flaggen		; oui, positionner le flag (broadcast)
	return			; fin du traitement

		; cas 2 : reception d'une donnee
		; -------------------------------
intssp2
	movf	ssptemp,w	; charger bits utiles de SSPSTAT
	xorlw	B'00101001'	; buffer plein, contient une donnee
	btfss	STATUS,Z	; condition realisee ?
	goto	intssp3 	; non, examiner cas 3
	movf	ptrin,w 	; charger pointeur d'entree
	movwf	FSR 		; dans pointeur d'adresse
	movf	SSPBUF,w 	; charger donnee recue
	movwf	INDF 		; la placer dans le buffer d'entree
	incf	ptrin,f 	; incrementer le pointeur d'entree
	return 			; fin du traitement

		; cas 3 : emission de la premiere donnee
		; --------------------------------------
intssp3
	movf	ssptemp,w	; charger bits utiles de SSPSTAT
	xorlw	B'00001100'	; demande d'envoi, on vient de recevoir l'adresse
	btfss	STATUS,Z	; condition realisee ?
	goto	intssp4		; non, examiner cas 4
	movlw	LOW bufout+1 	; charger adresse du buffer d'emission + 1
	movwf	ptrout 		; le prochain octet sera le second du buffer out
	movf	bufout,w 	; charger le premier octet du buffer d'emission
	movwf	SSPBUF 		; dans le buffer I2C
	bsf	SSPCON,CKP 	; met fin a la pause, permet le transfert
	return 			; fin du traitement

		; cas 4 : emission d'une donnee quelconque
		; ----------------------------------------
intssp4
	movf	ssptemp,w 	; charger bits utiles de SSPSTAT
	xorlw	B'00101100' 	; demande d'envoi qui suit un autre envoi
	btfss	STATUS,Z 	; condition realisee ?
	goto	intssp5 	; non, examiner cas 5
	movf	ptrout,w 	; charger pointeur buffer de sortie
	movwf	FSR 		; dans pointeur d'adresse
	movf	INDF,w 		; charger octet a envoyer
	movwf	SSPBUF 		; le mettre dans le buffer de sortie
	bsf	SSPCON,CKP 	; libere l'horloge, permet le transfert
	incf	ptrout,f 	; incrementer pointeur de sortie
	return			; fin du traitement

		; cas 5 : reception du NOACK
		; ---------------------------
intssp5
	movf 	ssptemp,w	; charger bits utiles de SSPSTAT
	xorlw 	B'00101000'	; NOCAK recu
	btfss 	STATUS,Z	; condition realisee ?
	goto 	$ 		; non, reset PIC par debordement watchdog
	;; 	bsf 	flagout 	; signaler fin de lecture
	return 			; et retour