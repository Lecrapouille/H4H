;
; h4h.asm for H4H in /Users/crapouille
;
; Made by Quentin Quadrat
; Mail   <quentin.quadrat@free.fr>
;
; Started on  Thu Jun 29 17:35:11 2006 Quentin Quadrat
; Last update Wed Sep 13 14:23:51 2006 Quentin Quadrat
;

changequote([,])

	LIST      p=16F876A            ; D�finition de processeur
	#include <p16F876A.inc>        ; fichier include

	__CONFIG   _CP_OFF & _DEBUG_OFF & _CPD_OFF & _LVP_OFF & _BODEN_OFF & _PWRTE_ON & _WDT_ON & _HS_OSC

	;_CP_OFF	 Pas de protection
	;_DEBUG_OFF	 RB6 et RB7 en utilisation normale
	;_WRT_ENABLE_OFF Le programme ne peut pas ecrire dans la flash
	;_CPD_OFF        Memoire EEprom deprotegee
	;_LVP_OFF	 RB3 en utilisation normale
	;_BODEN_ON	 Reset tension en service
	;_PWRTE_ON	 Demarrage temporise
	;_WDT_ON	 Watchdog en service
	;_HS_OSC	 Oscillateur haute vitesse (4Mhz<F<20Mhz)

	include(macros.asm)
	include(variables.asm)

	org	0x000 		; Adresse de d�part apr�s reset
 	goto    init		; Initialiser

	include(interruption.asm)
	include(anc.asm)
	include(timer.asm)
	include(i2c.asm)
	include(reponse.asm)

	include(init.asm)
	include(mainloop.asm)

	END 			; directive fin de programme